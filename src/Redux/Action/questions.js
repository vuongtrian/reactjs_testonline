import { SET_QUESTION } from "./type";
import { request } from "../../Config/requests";
import { createAction } from "./index";

export const fetQuestion = (dispatch) => {
    request("https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/questions", "GET")
      .then((res) => {
        console.log(res);
        dispatch(createAction(SET_QUESTION, res.data));
      })
      .catch((err) => {
        console.log(err);
      });
};

fetQuestion();
