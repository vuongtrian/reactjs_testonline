import { SET_QUESTION } from "../Action/type";

let initialState = {
  questionList: [],
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
      case SET_QUESTION:
          return {...state, questionList:payload}
    default:
      return state;
  }
};

export default reducer;
