import { combineReducers, createStore, applyMiddleware } from "redux";
import questionList from "../Redux/Reducer/question"
import thunk from "redux-thunk";
import answers from "../Redux/Reducer/result"


const reducer = combineReducers({
  questionList,
  answers,
});

export const store = createStore(
  reducer,
  applyMiddleware(thunk),
  // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);