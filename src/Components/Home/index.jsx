import { Container, Grid, Typography, withStyles } from "@material-ui/core";
import React, { Component } from "react";
import style from "./style";
import { fetQuestion } from "../../Redux/Action/questions";
import { connect } from "react-redux";
import CheckExact from "../MultipleChoice";
import FillInBlank from "../FillInBlank";

class Home extends Component {
  renderQuestion() {
    return this.props.questionList.map((questionItem, index) => {
      return (
        <Grid key={index}>
          {questionItem.questionType === 1 ? <CheckExact questions={questionItem}/> : <FillInBlank questions={questionItem}/>} 
        </Grid>
      );
    });
  }

  render() {
    return (
      <div>
        <Container maxWidth="md">
          <Typography
            className={this.props.classes.title}
            variant="h3"
            component="h2"
            gutterBottom
          >
            KIỂM TRA ONLINE
          </Typography>
          <Grid>{this.renderQuestion()}</Grid>
        </Container>
      </div>
    );
  }
  componentDidMount() {
    this.props.dispatch(fetQuestion);
  }
}

const mapStateToProps = (state) => {
  return {
    questionList: state.questionList.questionList,
  };
};

export default connect(mapStateToProps)(
  withStyles(style, { withTheme: true })(Home)
);
