import {
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid,
  Radio,
  RadioGroup,
  Typography,
  withStyles,
} from "@material-ui/core";
import React, { Component } from "react";
import {connect} from "react-redux";
import style from "./style";
import {createAction} from "../../Redux/Action/index"
import {CHECK_ANSWER} from "../../Redux/Action/type"

class CheckExact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      DanhSachDapAn: {
        QuestionId: "",
        Answer: {
          content: "",
          exact: "",
        },
      },
    };
    console.log(this.state);
  }

  handleChange = (e) => {
    this.setState({
      DanhSachDapAn: {
        QuestionId: e.target.value.id,
        Answer: {
          content: e.target.value.content,
          exact: e.target.value.exact,
        },
      }
    })
  }

  renderAnswer() {
    return this.props.questions.answers.map((answerItem, index) => {
      return (
        <RadioGroup
          key={index}
          aria-label="gender"
          name="gender1"
          value={this.state.DanhSachDapAn.QuestionId}
          onChange={this.handleChange}
        >
          <FormControlLabel
            value={answerItem}
            control={<Radio />}
            label={answerItem.content}
          />
        </RadioGroup>
      );
    });
  }
  render() {
    return (
      <div>
        <Grid container>
          <Grid item fullWidth>
            <FormControl component="fieldset">
              <Typography variant="h5" component="h4" variant="h6">
                {this.props.questions.content}
              </Typography>
              {this.renderAnswer()}
            </FormControl>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default connect()(withStyles(style, { withTheme: true })(CheckExact));
