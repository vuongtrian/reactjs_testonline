import React, { Component } from "react";
import {
  Grid,
  TextField,
  Typography,
  withStyles,
} from "@material-ui/core";

class FillInBlank extends Component {
  render() {
    console.log(this.props.questions);
    return (
      <div>
        <Grid container>
          <Grid item fullWidth>
            <Typography variant="h6">{this.props.questions.content}</Typography>
            <form noValidate autoComplete="off">
              <TextField
                id="outlined-basic"
                label="Answer in here"
                variant="outlined"
              />
            </form>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default FillInBlank;
